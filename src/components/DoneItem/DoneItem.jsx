import React from 'react'
import { useNavigate } from 'react-router-dom'
import "./doneItem.css"

const DoneItem = ({ doneItem }) => {
  const navigate = useNavigate()

  return (
    <div
      className='doneItemContainer'
      onClick={() => navigate(`/todos/${doneItem.id}`)}
    >
      <div>
        {doneItem.text}
      </div>
    </div>
  )
}

export default DoneItem