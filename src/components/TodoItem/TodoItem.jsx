import React, { useEffect, useState } from 'react';
import { useTodos } from '../../hooks/useTodos';
import { EditOutlined, ExclamationCircleFilled } from "@ant-design/icons"
import { Button, Modal } from 'antd';
import './todoItem.css'

const TodoItem = ({ todoItem }) => {
  const { toggleTodo, deleteTodo, } = useTodos()
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState('');

  const { confirm } = Modal;

  const handleToggle = async () => {
    const updatedTodoItem = { ...todoItem, done: !todoItem.done }
    await toggleTodo(updatedTodoItem)
  }

  const handleDelete = async () => {
    await deleteTodo(todoItem.id)
  }

  const handleOk = async () => {
    setConfirmLoading(true);
    await toggleTodo({ ...todoItem, text: modalText })
    setConfirmLoading(false);
    handleCancel()
  };

  const handleCancel = () => {
    setOpen(false)
  }

  const showConfirmForDeletion = () => {
    confirm({
      title: 'Do you want to delete this todo item?',
      icon: <ExclamationCircleFilled style={{ color: 'red'}}/>,
      content: `${todoItem.text}`,
      async onOk() {
       await handleDelete()
      },
      onCancel() {},

    });
  };

  useEffect(() => {
    setModalText(todoItem.text)
  }, [open, todoItem])

  return (
    <div className='todoItemContainer'>
      <div
        className="todoItemText"
        onClick={() => handleToggle()}
        style={{ textDecoration: todoItem.done && 'line-through' }}
      >
        {todoItem.text}
      </div>
      <div className='buttonContainer'>
        <div
          className='button'
          onClick={() => showConfirmForDeletion()}
        >
          X
        </div>
        <div
          className='button'
          onClick={() => setOpen(true)}
        >
          <EditOutlined />
        </div>
      </div>
      <Modal
        title="TodoItem"
        open={open}
        confirmLoading={confirmLoading}
        
        onCancel={() => handleCancel()}
        footer={[
          <Button
            key="cancel"
            onClick={() => handleCancel()}
          >
            Cancel
          </Button>,
          <Button
            key="ok"
            type="primary"
            disabled={modalText.trim().length === 0 || modalText.trim() === todoItem.text}
            onClick={() => { handleOk() }}
          >
            OK
          </Button>
        ]}
      >
        <textarea
          className='textarea'
          value={modalText}
          onChange={(event) => setModalText(event.target.value)}
        />
      </Modal>
    </div>
  )
}

export default TodoItem