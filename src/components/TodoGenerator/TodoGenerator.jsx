import React, { useState } from 'react'
import "./todoGenerator.css"
import { v4 as uuidv4 } from 'uuid'
import { useTodos } from '../../hooks/useTodos';
import { Button } from 'antd';

const TodoGenerator = () => {
  const [todoItem, setTodoItem] = useState('')
  const { addTodo } = useTodos()

  const updateTodoItem = (event) => {
    setTodoItem(event.target.value)
  }

  const addTodoItem = (event) => {
    event.preventDefault()
    const newTodoItem = {
      id: uuidv4(),
      text: todoItem,
      done: false
    }
    addTodo(newTodoItem)
  }

  return (
    <div className='todoGeneratorContainer'>
      <input
        className='todoInput'
        value={todoItem}
        type='text'
        placeholder='input a new todo here...'
        onChange={(event) => updateTodoItem(event)}
      />
      <Button
        type="primary"
        onClick={(event) => addTodoItem(event)}
        disabled={todoItem.trim().length === 0}
      >
        add
      </Button>
    </div>
  )
}

export default TodoGenerator