import React from 'react'
import './todoGroup.css'
import { useSelector } from 'react-redux'
import TodoItem from '../TodoItem/TodoItem'

const TodoGroup = () => {
  const todoListFromStore = useSelector((state) => state.todoList.todoList)

  return (
    <div className='todoGroupContainer'>
      {
        todoListFromStore.map((todoItem) => (
          <TodoItem key={todoItem.id} todoItem={todoItem} />
        )
        )
      }
    </div>
  )
}

export default TodoGroup