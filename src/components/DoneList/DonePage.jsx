import React from 'react'
import { useSelector } from 'react-redux'
import DoneItem from '../DoneItem/DoneItem'
import './donePage.css'

const DoneList = () => {
  const todoListFromStore = useSelector((state) => state.todoList.todoList)

  return (
    <div className='donePageContainer'>
      <div className='title'>DoneList</div>
      <div className='doneListContainer'>
      {
        todoListFromStore
          .filter((todoItem) => todoItem.done)
          .map((doneItem) => (
            <DoneItem key={doneItem.id} doneItem={doneItem} />
          ))
      }
      </div>
    </div>
  )
}

export default DoneList