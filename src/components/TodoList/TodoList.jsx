import React, { useEffect } from 'react'
import TodoGenerator from '../TodoGenerator/TodoGenerator'
import './todoList.css'
import TodoGroup from '../TodoGroup/TodoGroup'
import { useTodos } from '../../hooks/useTodos'

const TodoList = () => {
  const { loadTodos } = useTodos()

  useEffect(() => {
    loadTodos()
  })

  return (
    <div className='todoListContainer'>
      <div className='title'>TodoList</div>
      <TodoGroup />
      <TodoGenerator />
    </div>
  )
}

export default TodoList