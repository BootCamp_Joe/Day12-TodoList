import React from 'react'
import { useParams } from 'react-router-dom';
import './todoDetail.css'

const TodoDetail = () => {
  const { id } = useParams()

  return (
    <div className='idText'>
      ID: {id}
    </div>
  )
}

export default TodoDetail