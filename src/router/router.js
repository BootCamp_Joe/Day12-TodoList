import { createBrowserRouter } from 'react-router-dom';
import AboutPage from '../pages/AboutPage/AboutPage';
import DoneList from '../components/DoneList/DonePage';
import TodoList from '../components/TodoList/TodoList'
import Layout from '../layout/Layout';
import TodoDetail from '../components/TodoDetail/TodoDetail';
import NotFoundPage from '../pages/NotFoundPage/NotFoundPage'


const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    errorElement: <NotFoundPage />,
    children: [
      {
        index: true,
        element: <TodoList />
      },
      {
        path: "/about",
        element: <AboutPage />
      },
      {
        path: "/done",
        element: <DoneList />
      },
      {
        path: "/todos/:id",
        element: <TodoDetail />
      }
    ]
  },
])

export default router;