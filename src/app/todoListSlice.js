import {createSlice} from "@reduxjs/toolkit"

const initialState = {
  todoList: [],
}

const todoListSlice = createSlice({
  name: "TodoList",
  initialState,
  reducers: {
    updateTodoList: (state, action) => {
      state.todoList = action.payload
    },
    addTodoList: (state, action) => {
      state.todoList.push(action.payload)
    },
    toggleTodoList: (state, action) => {
      state.todoList = state.todoList.map((todoItem) => todoItem.id === action.payload.id? 
      action.payload
      : 
      todoItem)
    },
    deleteTodoList: (state, action) => {
      state.todoList = state.todoList.filter((todoItem) => todoItem.id !== action.payload.id)
    }
  }
})

export const {updateTodoList, toggleTodoList, deleteTodoList, addTodoList} = todoListSlice.actions
export default todoListSlice.reducer;