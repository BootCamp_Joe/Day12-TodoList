import { getTodos, addTodos, updateTodos, deleteTodos } from "../api/todos"
import { useDispatch } from "react-redux"
import { updateTodoList } from "../app/todoListSlice";

export const useTodos = () => {
  const dispatch = useDispatch();

  const loadTodos = () => {
    getTodos().then((response) => {
      dispatch(updateTodoList(response.data))
    })
  }

  const addTodo = async (todoItem) => {
    await addTodos(todoItem)
    loadTodos();
  }

  const toggleTodo = async (todoItem) => {
    await updateTodos(todoItem)
    loadTodos();
  }

  const deleteTodo = async (todoItemId) => {
    await deleteTodos(todoItemId)
    loadTodos();
  }

  return {
    loadTodos, 
    addTodo, 
    toggleTodo, 
    deleteTodo
  }
}