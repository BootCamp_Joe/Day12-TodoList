import React from 'react'
import './notFoundPage.css'

const NotFoundPage = () => {
  return (
    <div className='notFoundText'>
      Page Not Found
    </div>
  )
}

export default NotFoundPage