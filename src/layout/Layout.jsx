import {Outlet} from "react-router-dom"
import Navigation from "./Navigation"
import './layout.css'

const Layout = () => {
  return (
    <>
      <div className="navBar">
          <div className="appName">
            My Todo List
          </div>
          <Navigation />
      </div>
      <Outlet />
    </>
  )
}

export default Layout