import { Link } from "react-router-dom"
import './navigation.css'

const Navigation = () => {
  return (
    <nav>
      <ul className="navigationList">
        <li>
          <Link to={"/"}>Home</Link>
        </li>
        <li>
          <Link to={"/done"}>Done</Link>
        </li>
        <li>
          <Link to={"/about"}>About</Link>
        </li>
      </ul>
    </nav>

  )
}

export default Navigation